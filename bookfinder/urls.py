from django.contrib import admin
from django.urls import path
from . import views

app_name = 'bookfinder'

urlpatterns = [
	path('', views.bookfinder, name='bookfinder'),
]
